# Just a VM!


## What does this do?

This just makes a VM for you. In AWS, making "just a VM" requires other nonsense like a VPC, route tables, etc. This repo makes all that noise for you so you can quickly stand up a VM for your needs.

## What else does it do?

What else does it _need_ to do? If you need more, feel free to fork & add. This is deliberately a simple project.

## How do I use it?

Clone the repo, personalize the file for your needs (security groups, AMI, region, etc), and go!